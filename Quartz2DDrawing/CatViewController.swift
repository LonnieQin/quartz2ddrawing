//
//  CatViewController.swift
//  Quartz2DDrawing
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit

class CatViewController: UIViewController {
    let STEP:CGFloat = 10.0
    @IBOutlet weak var catView: CatView!
    
    override func viewWillAppear(animated: Bool) {

        super.viewWillAppear(animated)
        self.navigationController?.toolbarHidden = false
        print("nav:\(self.navigationController) \(self.navigationController!.toolbar)");
    }
    
    @IBAction func left(sender: AnyObject) {
       catView.point = CGPointMake(catView.point.x-STEP, catView.point.y)
       catView.setNeedsDisplay()
    }
    @IBAction func up(sender: AnyObject) {
       catView.point = CGPointMake(catView.point.x, catView.point.y+STEP)
       catView.setNeedsDisplay()
    }
    @IBAction func right(sender: AnyObject) {
      catView.point = CGPointMake(catView.point.x+STEP, catView.point.y)
      catView.setNeedsDisplay()
    }
    @IBAction func down(sender: AnyObject) {
      catView.point = CGPointMake(catView.point.x, catView.point.y-STEP)
       catView.setNeedsDisplay()
    }
    
    @IBAction func add(sender: AnyObject) {
        catView.scale += 0.1
        catView.setNeedsDisplay()
    }
    
    @IBAction func minus(sender: AnyObject) {
        catView.scale -= 0.1
        catView.setNeedsDisplay()
    }
    
    @IBAction func rotate(sender: AnyObject) {
        catView.angle += 10
        catView.setNeedsDisplay()
    }
    
    
}
