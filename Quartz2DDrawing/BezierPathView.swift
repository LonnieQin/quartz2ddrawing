//
//  BezierPathView.swift
//  Quartz2DDrawing
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit
@IBDesignable
class BezierPathView: UIView {
    override func drawRect(rect: CGRect) {
        let ctx = UIGraphicsGetCurrentContext()
        CGContextMoveToPoint(ctx, 333, 0)
        CGContextAddCurveToPoint(ctx, 333, 0, 332, 26, 330, 26)
        CGContextAddCurveToPoint(ctx, 330, 26, 299, 20, 299, 17)
        CGContextAddLineToPoint(ctx, 296, 17)
        CGContextAddCurveToPoint(ctx, 296, 17, 296, 19, 291, 19)
        CGContextAddLineToPoint(ctx, 250, 19)
        CGContextAddCurveToPoint(ctx, 250, 19, 241, 244, 338, 19)
        CGContextAddCurveToPoint(ctx, 236, 20, 234, 24, 227, 24)
        CGContextAddCurveToPoint(ctx, 220, 24, 217, 19, 216, 19)
        CGContextAddCurveToPoint(ctx, 214, 20, 211, 22, 207, 20)
        CGContextAddCurveToPoint(ctx, 207, 20, 187, 20, 182, 21)
        CGContextAddLineToPoint(ctx, 100, 45)
        CGContextAddLineToPoint(ctx, 97, 46)
        CGContextAddCurveToPoint(ctx, 97, 46, 86, 71, 64, 72)
        CGContextAddCurveToPoint(ctx, 42, 74, 26, 56, 23, 48)
        CGContextAddLineToPoint(ctx, 9, 47)
        CGContextAddCurveToPoint(ctx, 9, 47, 0, 31, 0, 0)
        CGContextStrokePath(ctx)
    }
}
