//
//  ViewController.swift
//  Quartz2DDrawing
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var titles = ["FillingRect","DrawingDog","StrokedFilledTriangle","BeizerPathDemo","Cat"]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController
        let indexPath = sender
        vc.title = titles[indexPath!.row]
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("simple")
        cell!.textLabel?.text = titles[indexPath.row]
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("indexpath:\(indexPath)")
        performSegueWithIdentifier(String(indexPath.row), sender:indexPath)
    }
    
    

}

