//
//  CatView.swift
//  Quartz2DDrawing
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit

@IBDesignable
class CatView: UIView {
    var point:CGPoint = CGPointZero
    var scale:CGFloat = 1.0
    var angle:Double = Double(0.0)
    override func drawRect(rect: CGRect) {
        // Drawing code
        let path = NSBundle.mainBundle().pathForResource("cat", ofType: "jpg")
        let img = UIImage(contentsOfFile: path!)
        let cgImage = img!.CGImage
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        
        CGContextTranslateCTM(context,0,img!.size.height/6)
        CGContextScaleCTM(context, 1, -1)

        var myAffine = CGAffineTransformMakeTranslation(point.x, point.y)
        myAffine = CGAffineTransformScale(myAffine, scale, scale)
        myAffine = CGAffineTransformRotate(myAffine, CGFloat(self.angle*M_PI / 180.0))
        CGContextConcatCTM(context, myAffine)
        
        let touchRect = CGRectMake(0, 0, img!.size.width/6, img!.size.height/6)
        CGContextDrawImage(context, touchRect, cgImage)
        CGContextRestoreGState(context)
    }
}
