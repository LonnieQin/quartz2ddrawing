//
//  DrawView.swift
//  StrokedFillledTriangle
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit
@IBDesignable
class DrawView: UIView {
    override func drawRect(rect: CGRect) {
        // Drawing code
        let context = UIGraphicsGetCurrentContext()
        CGContextMoveToPoint(context, 75, 10)
        CGContextAddLineToPoint(context, 10, 150)
        CGContextAddLineToPoint(context, 160, 150)
        CGContextClosePath(context)
        
        //设置黑色描边参数
        UIColor.blackColor().setStroke()
        UIColor.greenColor().setFill()
        CGContextSaveGState(context)
        UIColor.redColor().setFill()
        CGContextRestoreGState(context)
        CGContextDrawPath(context, .Stroke)
    }

}
