//
//  FillingView.swift
//  FillingRect
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit
@IBDesignable
class FillingView: UIView {
    override func drawRect(rect: CGRect) {
        // Drawing code
        UIColor.brownColor().setFill()
        UIRectFill(rect)
        
        UIColor.whiteColor().setStroke()
        let frame = CGRectMake(20, 30, 100, 300)
        UIRectFrame(frame)
    }
}
