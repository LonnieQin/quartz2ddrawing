//
//  DogView.swift
//  DrawingDog
//
//  Created by apple on 15/10/30.
//  Copyright © 2015年 apple. All rights reserved.
//

import UIKit
@IBDesignable
class DogView: UIView {
    override func drawRect(rect: CGRect) {
        let imagePath = NSBundle.mainBundle().pathForResource("dog", ofType: "jpg")
        let image = UIImage(contentsOfFile: imagePath!)
        image?.drawInRect(CGRectMake(0, 80, 320, 400))
        let title:NSString = "我的小狗"
        let font = UIFont.systemFontOfSize(34)
        let attr = [NSFontAttributeName:font]
        title.drawAtPoint(CGPointMake(100, 20), withAttributes: attr)
    }
}
